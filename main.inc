; --- Externals:
    Extrn  GetModuleHandleA : proc, DialogBoxParamA : proc
    Extrn  MessageBoxA : proc, SendMessageA : proc
    Extrn  ExitProcess : proc, EndDialog : proc
    Extrn  FindFirstFileA : proc, LoadIconA : proc
    Extrn  lstrcpyA : proc, lstrcatA : proc,lstrcmpiA : proc
    Extrn  FindNextFileA : proc, FindClose : proc
    Extrn  DeleteFileA : proc, GetSystemDirectoryA : proc
    Extrn  SetDlgItemTextA : proc
    Extrn  RegDeleteKeyA : proc, RegDeleteValueA : proc,RegCreateKeyA : proc
    Extrn  RegOpenKeyExA : proc, RegCloseKey : proc, RegQueryValueExA : proc, RegQueryValueA : proc
    Extrn  lstrcatA : proc
    Extrn  _lclose : proc, _lopen : proc, _lwrite : proc, _lread : proc
    Extrn  SetEndOfFile : proc, _llseek : proc, GetFileSize : proc

; -----------------------------------------------
    OF_READWRITE           equ 2h
    SEEK_SET               equ 0
; -----------------------------------------------
    KEY_WRITE              equ (STANDARD_RIGHTS_WRITE OR KEY_SET_VALUE OR KEY_CREATE_SUB_KEY) AND NOT SYNCHRONIZE
    STANDARD_RIGHTS_WRITE  equ READ_CONTROL
    READ_CONTROL           equ 20000h
    KEY_SET_VALUE          equ 2h
    KEY_CREATE_SUB_KEY     equ 4h
    SYNCHRONIZE            equ 100000h
; -----------------------------------------------
    KEY_READ               equ (STANDARD_RIGHTS_READ OR KEY_QUERY_VALUE OR KEY_ENUMERATE_SUB_KEYS OR KEY_NOTIFY) AND NOT SYNCHRONIZE
    STANDARD_RIGHTS_READ   equ READ_CONTROL
    READ_CONTROL           equ 20000h
    KEY_QUERY_VALUE        equ 1h
    KEY_ENUMERATE_SUB_KEYS equ 8h
    KEY_NOTIFY             equ 10h
; -----------------------------------------------
    ;KEY_READ or KEY_WRITE == 2001Fh
    HKEY_CLASSES_ROOT      equ 80000000h
    HKEY_CURRENT_USER      equ 80000001h
    HKEY_LOCAL_MACHINE     equ 80000002h
    HKEY_USERS             equ 80000003h
    HKEY_PERFORMANCE_DATA  equ 80000004h
    HKEY_CURRENT_CONFIG    equ 80000005h
    HKEY_DYN_DATA          equ 80000006h
; -----------------------------------------------


